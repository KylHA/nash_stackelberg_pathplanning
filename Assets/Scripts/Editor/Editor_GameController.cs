﻿using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(GameController))]
public class Editor_GameController : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        GameController myScript = (GameController)target;

        if (GUILayout.Button("Build Object"))
            myScript.Editor_CreateBlocks();

        if (GUILayout.Button("Link Object"))
            myScript.Editor_LinkObjects();
    }
}