﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadDataController : MonoBehaviour
{
    public GameObject PZObj, PXObj, NZObj, NXObj;
    public List<GameObject> myObjects;

    public bool isReserved;
    public GameObject reservedObj;
    public int myRoadID;

    public void SetRoadLinks()
    {
        myObjects = new List<GameObject>();

        isReserved = false;
        reservedObj = null;

        Collider myCold = GetComponent<Collider>();
        myCold.enabled = false;

        RaycastHit hit;

        if (Physics.Raycast(new Ray(transform.position, transform.forward), out hit))
            PZObj = hit.collider.gameObject;

        if (Physics.Raycast(new Ray(transform.position, transform.right), out hit))
            PXObj = hit.collider.gameObject;

        if (Physics.Raycast(new Ray(transform.position, -transform.forward), out hit))
            NZObj = hit.collider.gameObject;

        if (Physics.Raycast(new Ray(transform.position, -transform.right), out hit))
            NXObj = hit.collider.gameObject;

        if (PZObj != null)
            myObjects.Add(PZObj);

        if (PXObj != null)
            myObjects.Add(PXObj);

        if (NZObj != null)
            myObjects.Add(NZObj);

        if (NXObj != null)
            myObjects.Add(NXObj);

        myCold.enabled = true;
    }

    private Vector3 Forward() { return (transform.forward).normalized * 2f; }
    private Vector3 Backward() { return (-transform.forward).normalized * 2f; }
    private Vector3 Right() { return (transform.right).normalized * 2f; }
    private Vector3 Left() { return (-transform.right).normalized * 2f; }
}