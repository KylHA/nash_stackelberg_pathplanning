﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class RoadFollowerBehavior : MonoBehaviour
{

    #region Cached

    private int myID;
    private float mySpeed = 5f;
    [SerializeField] private float myTotalDistance = 0;
    [SerializeField] private Transform myRaycastTarget;

    #endregion

    #region Created
    [SerializeField] private GameObject myFinalTarget;
    private List<GameObject> myNashTargetRoad;
    private List<GameObject> myClosestTargetRoad;
    #endregion

    #region Dynamic
    private GameObject myreservedRoad;
    private GameObject myCurrentRoad;
    RoadDataController currentRoadController;
    #endregion

    #region GET/SET

    public int agentID { get { return myID; } set { this.myID = value; } }
    public float agentSpeed { get { return mySpeed; } set { this.mySpeed = value; } }
    public GameObject agentCurrentObj { get { return myCurrentRoad; } set { this.myCurrentRoad = value; } }
    public GameObject agentNextObj { get { return myreservedRoad; } set { this.myreservedRoad = value; } }
    public GameObject agentFinalTarget { get { return myFinalTarget; } set { this.myFinalTarget = value; } }
    public List<GameObject> agentClosestRoad { get { return myClosestTargetRoad; } }
    public List<GameObject> agentNashRoad { get { return myNashTargetRoad; } }

    #endregion

    private void OnMouseDown()
    {
        ShowClosestRoad();
        ShowNashRoad();
    }

    private void ShowClosestRoad()
    {
        if (myClosestTargetRoad.Count < 1)
            Debug.LogError("Agent_" + myID + "Closest Road not Calculated");

        else
            foreach (var item in myClosestTargetRoad)
                item.GetComponent<Renderer>().material.color = Color.yellow;
    }

    private void ShowNashRoad()
    {
        if (myNashTargetRoad.Count < 1)
            Debug.LogError("Agent_" + myID + "Nash Road not Calculated");


        else
        {
            CalcClosestRoad();

            foreach (var item in myClosestTargetRoad)
                item.GetComponent<Renderer>().material.color = GetComponent<Renderer>().material.color;
        }
    }

    private void CalcClosestRoad()
    {


        GameObject myForward = Physics.RaycastAll(myRaycastTarget.position, myRaycastTarget.forward, 5f)[0].collider.gameObject;

        currentRoadController = myRaycastTarget.GetComponent<RoadDataController>();

        myClosestTargetRoad.Add(currentRoadController.gameObject);

        while (currentRoadController.PXObj != null)
        {
            myClosestTargetRoad.Add(currentRoadController.gameObject);
        }
    }

    public void CalcNashRoad()
    {
        List<float> nashTraverseObjList = new List<float>();

        foreach (var item in myClosestTargetRoad)
        {
            if (item.GetComponent<RoadDataController>().isReserved)
            {
                //nashTraverseObjList.Add();
                RoadFollowerBehavior otherAgent = item.GetComponent<RoadDataController>().reservedObj.GetComponent<RoadFollowerBehavior>();

                float otherTVal = CalcTVal(otherAgent.agentClosestRoad.Count, agentSpeed);
                float myTval = CalcTVal(myClosestTargetRoad.Count, mySpeed);

                if (otherTVal < myTval)
                    nashTraverseObjList.Add(mySpeed);
                else
                    nashTraverseObjList.Add(CalcNextSpeed(myTval, otherTVal));
            }
            else
            {
                nashTraverseObjList.Add(mySpeed);
            }
        }
    }

    float CalcTVal(float _totalDist, float _currentSpeed)
    {
        return (_totalDist / _currentSpeed);
    }

    public float CalcNextSpeed(float myTval, float otherTval)
    {
        return mySpeed * (1 - (otherTval - myTval)*.1f);
    }

    private RoadDataController HitObject(RoadDataController _current, GameObject _target)
    {
        RoadDataController tmp = null;
        _current.GetComponent<Collider>().enabled = false;

        Ray ray = new Ray(_current.transform.position, Direction(_target.transform.position, _current.transform.position));
        RaycastHit hit;

        _current.GetComponent<Collider>().enabled = true;

        return tmp;
    }


    Vector3 Direction(Vector3 _firsPos, Vector3 _secondPos)
    {
        return (_secondPos - _firsPos).normalized;
    }
}