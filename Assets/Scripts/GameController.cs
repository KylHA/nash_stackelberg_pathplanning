﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController instance;

    public GameObject cubePrefab;
    public GameObject roadHolderParent;

    private List<List<GameObject>> blockList;


    

    public void Awake()
    {
        instance = this;
    }
    #region CONTROLLER

    #endregion

    #region EDITOR

    public void Editor_CreateBlocks()
    {
        float xCoor = 0;
        float yCoor = 0;
        int roadID = 0;

        blockList = new List<List<GameObject>>();

        for (int i = 0; i < 30; i++)
        {
            List<GameObject> tmpList = new List<GameObject>();

            for (int j = 0; j < 30; j++)
            {
                GameObject tmpObj = Instantiate(cubePrefab);

                tmpObj.name = "Road_" + roadID;
                tmpObj.GetComponent<RoadDataController>().myRoadID = roadID;
                tmpObj.transform.position = new Vector3(xCoor, 0, yCoor);
                roadID++;

                xCoor += 1.5f;
                tmpList.Add(tmpObj);
            }
            xCoor = 0;
            yCoor += 1.5f;

            blockList.Add(tmpList);
        }

        roadHolderParent.transform.position = new Vector3((blockList[0][0].transform.position.x + blockList[blockList.Count - 1][blockList[0].Count - 1].transform.position.x) / 2, 0,
            (blockList[0][0].transform.position.z + blockList[blockList.Count - 1][blockList[0].Count - 1].transform.position.z) / 2);

        foreach (var itemRow in blockList)
            foreach (var itemCol in itemRow)
            {
                itemCol.GetComponent<RoadDataController>().SetRoadLinks();
                itemCol.transform.parent = roadHolderParent.transform;
            }
    }

    public void Editor_LinkObjects()
    {
        foreach (Transform item in roadHolderParent.transform)
        {
            item.GetComponent<RoadDataController>().SetRoadLinks();
        }
    }

    #endregion
}
//1.75